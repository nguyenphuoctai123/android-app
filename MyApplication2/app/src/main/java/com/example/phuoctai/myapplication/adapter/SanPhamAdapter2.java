package com.example.phuoctai.myapplication.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.phuoctai.myapplication.R;
import com.example.phuoctai.myapplication.model.Sanpham;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SanPhamAdapter2 extends RecyclerView.Adapter<SanPhamAdapter2.ViewHolder>{
    private ArrayList<Sanpham> arraysanpham;
    private Context context;

    public SanPhamAdapter2(ArrayList<Sanpham> arraysanpham, Context context) {
        this.arraysanpham = arraysanpham;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.dong_sanphammoinhat,viewGroup,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.txttenSanpham.setText(arraysanpham.get(i).getTensanpham());
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        viewHolder.txtgiasanpham.setText("Giá : " + decimalFormat.format(arraysanpham.get(i).getGiassanpham()) + " Đ");
        Picasso.with(context).load(arraysanpham.get(i).getHinhanhsanpham())
                .placeholder(R.drawable.noimage)
                .error(R.drawable.error)
                .into(viewHolder.imghinhsanpham);
    }
    @Override
    public int getItemCount() {
        return arraysanpham.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imghinhsanpham;
        private TextView txttenSanpham, txtgiasanpham;

        public ViewHolder(View itemView) {
            super(itemView);
            imghinhsanpham = (ImageView) itemView.findViewById(R.id.imageviewsanpham);
            txtgiasanpham = (TextView) itemView.findViewById(R.id.textviewgiasanpham);
            txttenSanpham = (TextView) itemView.findViewById(R.id.textviewtensanpham);
        }
    }
}
