package com.example.phuoctai.myapplication.activity;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.phuoctai.myapplication.R;
import com.example.phuoctai.myapplication.adapter.LoaispAdapter;
import com.example.phuoctai.myapplication.adapter.SanPhamAdapter2;
import com.example.phuoctai.myapplication.model.Loaisp;
import com.example.phuoctai.myapplication.model.Sanpham;
import com.example.phuoctai.myapplication.ultil.CheckConnection;
import com.example.phuoctai.myapplication.ultil.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewFlipper viewFlipper;
    RecyclerView recyclerView;

    NavigationView navigationview;
    ListView listviewmanhinhchinh;

    DrawerLayout drawerLayout;
    ArrayList<Loaisp> mangloaisp;
    LoaispAdapter loaispAdapter;

    int id = 0;
    String tenloaisp = "";
    String hinhanhloaisp = "";
    ArrayList<Sanpham> mangsanpham;
    SanPhamAdapter2 sanphamAdapter;
    ImageView imageview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        Actionbar();
        ActionViewfliper();
        if (CheckConnection.haveNetworkConnection(getApplicationContext())){
            Actionbar();
            ActionViewfliper();
            Getdulieuloaisp();

        }else {
            CheckConnection.ShowToast_Short(getApplicationContext(),"ban hay kiem tra lai ket noi");
            finish();
        }
        GetDuLieuSPMoiNhat();
    }

    private void GetDuLieuSPMoiNhat() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Server.DuongdanloaispMoinhat, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    if (response != null){
                        int ID;
                        String Tensanpham;
                        Integer Giasanpham;
                        String Hinhanhsanpham;
                        String Motasanpham;
                        int IDsanpham = 0;
                        for (int i =0; i<response.length(); i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                ID = jsonObject.getInt("id");
                                Tensanpham = jsonObject.getString("tenloaisanpham");
                                Giasanpham = jsonObject.getInt("giasp");
                                Hinhanhsanpham = jsonObject.getString("hinhanhsp");
                                Motasanpham = jsonObject.getString("motasp");
                                IDsanpham = jsonObject.getInt("idsanpham");
                                Log.d("cccccc",Tensanpham);
                                mangsanpham.add(new Sanpham(ID, Tensanpham, Giasanpham, Hinhanhsanpham, Motasanpham, IDsanpham));
                                sanphamAdapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }else {
                        Toast.makeText(MainActivity.this, "Rỗng", Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    CheckConnection.ShowToast_Short(getApplicationContext(),error.toString());
                    Log.d("ccccc","ErActionBarror");
                }
            });
            requestQueue.add(jsonArrayRequest);

        }

    }

    private void Getdulieuloaisp() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Server.Duongdanloaisp, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (response != null){
                    for (int i =0; i<response.length(); i++){
                        try {
                            JSONObject jsonObject = response.getJSONObject(i);
                            id = jsonObject.getInt("id");
                            tenloaisp = jsonObject.getString("tenloaisp");
                            hinhanhloaisp = jsonObject.getString("hinhanhloaisp");
                            mangloaisp.add(new Loaisp(id, tenloaisp, hinhanhloaisp));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }// Nó báo lỗi list đấy rỗng. A xem cái thêm dữ liệu bên này này. nó ko thêm vào đc
                    }

                    mangloaisp.add(new Loaisp(0,"Lien He","http://phukientrangtrisinhnhat.com/wp-content/uploads/2017/08/Phone-icon.jpg"));
                    mangloaisp.add(new Loaisp(0,"Thong Tin","http://www.thegioithietkeweb.com:7777/mediaroot/media/userfiles/useruploads/4/image/thanh-toan/1.jpg"));
                    loaispAdapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                CheckConnection.ShowToast_Short(getApplicationContext(),error.toString());
            }
        });
        requestQueue.add(jsonArrayRequest);

    }

    private void ActionViewfliper() {
        ArrayList<String> mangquangcao = new ArrayList<>();
        mangquangcao.add("https://tinhte.cdnforo.com/store/2014/08/2572609_Hinh_2.jpg");
        mangquangcao.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_k3sDnXnO2bnqJID8RJ3SYT4d3Bnt87vOgiG6rtz3xgHG5eEC");
        mangquangcao.add("https://photo2.tinhte.vn/data/attachment-files/2018/11/4485252_Tinhte_C211.jpg");
        mangquangcao.add("https://photo2.tinhte.vn/data/attachment-files/2018/11/4485250_Tinhte_C29.jpg");
        for (int i=0; i<mangquangcao.size(); i++) {
            ImageView imageView = new ImageView(getApplicationContext());
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            Glide.with(this).load(mangquangcao.get(i)).into(imageView);
            viewFlipper.addView(imageView);

//        for (int i=0; i<mangquangcao.size(); i++) {
//            ImageView image = ic_new ImageView(this);
//            image.setScaleType(ImageView.ScaleType.FIT_XY);
//            Glide.with(this).load(mangquangcao.get(0)).into(image);
//            viewFlipper.addView(image);
//        }
        viewFlipper.setFlipInterval(5000);
        viewFlipper.setAutoStart(true);
        Animation animation_slide_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right);
        Animation animation_slide_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_right);
        viewFlipper.setInAnimation(animation_slide_in);
        viewFlipper.setOutAnimation(animation_slide_out);
    }}

    private void Actionbar() {


    setSupportActionBar(toolbar);
    getSupportActionBar ().setDisplayHomeAsUpEnabled(true);
    toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    });
    }
    private void Anhxa() {
        toolbar = (Toolbar) findViewById(R.id.toolbarmanhinhchinh);
        viewFlipper =(ViewFlipper) findViewById(R.id.viewflipper);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        navigationview = (NavigationView) findViewById(R.id.navigationview);
        listviewmanhinhchinh = (ListView) findViewById(R.id.lisviewmanhinhchinh);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerlayout);
        mangloaisp = new ArrayList<>();
        mangloaisp.add(0,new Loaisp(0,"Trang Chinh","https://www.rawshorts.com/freeicons/wp-content/uploads/2017/01/orange_petspictdoghouse_1484336163.png"));
        loaispAdapter = new LoaispAdapter(mangloaisp, getApplicationContext());
        listviewmanhinhchinh.setAdapter(loaispAdapter);
        mangsanpham = new ArrayList<>();
        sanphamAdapter = new SanPhamAdapter2(mangsanpham,getApplicationContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        recyclerView.setAdapter(sanphamAdapter);
    }
}
// VÃI. Sai ngay từ đầu ==' a đã khởi tạo cái arraylist đâu :V